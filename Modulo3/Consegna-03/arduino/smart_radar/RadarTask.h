#ifndef __RADARTASK__
#define __RADARTASK__

#include <Servo.h>
#include "Task.h"
#include "Led.h"
#include "Sonar.h"

class RadarTask: public Task {

  private:
    int servoPin;
    int trigPin;
    int echoPin;
    int ledPin;
    int servoPos;
    Servo servoScanner;
    Light* ledConnected;
    Sonar* proxSensor;
    enum {RIGHT, LEFT} servoDirection;
    enum {IDLE, SCANNING, TRACKING} state;
    void sendProxDistance();
    void boot();
    
  public:
    RadarTask(int servoPin, int trigPin, int echoPin, int ledPin);
    void init(int period);
    void tick();
};

#endif
