#include <Servo.h>
#include "Arduino.h"
#include "RadarTask.h"
#define INIT_POS 90

RadarTask::RadarTask(int servoPin, int trigPin, int echoPin, int ledPin) {
  this->servoPin = servoPin;
  this->trigPin = trigPin;
  this->echoPin = echoPin;
  this->ledPin = ledPin;
}

void RadarTask::init(int period) {
  Task::init(period);
  Serial.begin(9600);
  servoScanner.attach(servoPin);
  proxSensor = new Sonar(echoPin, trigPin);
  ledConnected = new Led(ledPin);
  boot();
}

void RadarTask::sendProxDistance() {
    float distance;
    distance = proxSensor->getDistance();
    Serial.print("[");
    Serial.print(distance);
    Serial.print(" ");
    Serial.print(servoPos);
    Serial.println("]");
}

void RadarTask::boot() {
  servoPos = INIT_POS;
  servoScanner.write(servoPos);
  ledConnected->switchOff();
  state = IDLE;
}

void RadarTask::tick() {
  switch(state) {
    case IDLE:
      if(Serial.available()) {
        char data = Serial.read();
        if(data == 'R') {
          servoPos = 0;
          servoScanner.write(servoPos);
          servoDirection = RIGHT;
          ledConnected->switchOn();
          servoScanner.attach(servoPin);
          state = SCANNING;
        }
      }
      break;
    case SCANNING:
      if(servoDirection == RIGHT) {
        servoScanner.write(++servoPos);
        if(servoPos == 180) {
          Serial.println("F");
          servoDirection = LEFT;
        }
      } else if(servoDirection == LEFT) {
        servoScanner.write(--servoPos);
        if(servoPos == 0) {
          Serial.println("F");
          servoDirection = RIGHT;
        }
      }
      sendProxDistance();   
      if(Serial.available()) {
        char data = Serial.read();
        if(data == 'T') {
          servoScanner.detach();
          state = TRACKING;
        } else if(data == 'I') {
          boot();
        }
      }
      break;
    case TRACKING:
      sendProxDistance();
      if(Serial.available()) {
        char data = Serial.read();
        if(data == 'S') {
          servoScanner.attach(servoPin);
          state = SCANNING;
        } else if(data == 'I') {//se occorre questo evento, nel metodo di boot() andiamo a fare servoScanner.write(servoPos) senza aver fatto la attach. Per arrivare in questo stato, prima abbiamo dovuto fare la detach. Quindi può darsi che il sistema non funzioni più.
          boot();
        }
      }
      break;
  }
}
