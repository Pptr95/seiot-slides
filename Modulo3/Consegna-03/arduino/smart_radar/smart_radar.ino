#include "FlexiTimer2.h"
#include "RadarTask.h"

#define OMEGA 80

RadarTask *radarTask = new RadarTask(9, 7, 4, 2);   //servo, trig, echo, led

void runTask() {
  radarTask->tick();
}

void setup() {
  radarTask->init(OMEGA);
  FlexiTimer2::set(OMEGA, runTask);
  FlexiTimer2::start();
}

void loop() {
}
