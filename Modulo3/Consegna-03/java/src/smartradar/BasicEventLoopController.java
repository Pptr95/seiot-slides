package smartradar;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import interfaces.Event;
import interfaces.Observer;
import utils.Observable;
/*Questa architettura dice "voglio mettere in campo un agente che funzioni più ad eventi al posto del solito polling, quindi
 che reagisce a degli eventi senza dover fare polling".
 BasicEventLoopController è sostanzialmente un thread che è implementato internamente per avere la capacità di gestire
 degli eventi che gli vengono notificati da componeti passivi (oppure anche attivi nel caso di messaggi) e mette a disposizione
 un metodo che bisogna ridefinire che è processEvent.
 Internamente ha una coda degli eventi dove gli vengono notificati eventi che possono essere di interesse per lui.
 Quindi è come se fosse un componente attivo che mette a disposizione al mondo esterno, per interagire, a livello di interfaccia
 un solo evento che è notifyEvent per dirgli che nuovi eventi sono occorsi.
 Questo ha senso farlo quando progettiamo un sistema dove, ad esempio, il pulsante lo iniziamo a vedere come GENERATORE DI EVENTI.
 Quindi in questa versione event, il bottone non è più solo un componete che mi mette a disposizione isPressed per vedere se è premuto
 o meno, ma lo possiamo organizzare come un pattern Observer come effettivamente una entità in grado di generare eventi.
 Questo è caratterizzato dall'interfaccia Observable: Observable è un qualsisi componente passivo che può generare degli eventi.*/
public abstract class BasicEventLoopController extends Thread implements Observer {
	
	public static final int defaultEventQueueSize = 50;
	protected BlockingQueue<Event> eventQueue;
	
	protected BasicEventLoopController(int size){
		eventQueue = new ArrayBlockingQueue<Event>(size);
	}

	protected BasicEventLoopController(){
		this(defaultEventQueueSize);
	}
	
	abstract protected void processEvent(Event ev);
	
	public void run(){
		while (true){
			try {
                Event ev = this.waitForNextEvent(); /* qui si blocca, non fa un busy waiting dove si controlla ogni volta se c'è un elemento in coda, ma take() (vedere sotto) è metodo che sospende il processo fino a quando non c'è almeno un evento in coda*/
				this.processEvent(ev);				
			} catch (Exception ex){				
				ex.printStackTrace();
			}
		}
	}
	
	protected void startObserving(Observable object){
		object.addObserver(this);
	}

	protected void stopObserving(Observable object){
		object.removeObserver(this);
	}
	
	protected Event waitForNextEvent() throws InterruptedException {
		return eventQueue.take();
	}

	protected Event pickNextEventIfAvail() throws InterruptedException {
		return eventQueue.poll();
	}
	
	public boolean notifyEvent(Event ev){
		return eventQueue.offer(ev);
	}
}
