package devices;

import interfaces.Button;
import utils.Observable;

public abstract class ObservableButton extends Observable implements Button {

}
