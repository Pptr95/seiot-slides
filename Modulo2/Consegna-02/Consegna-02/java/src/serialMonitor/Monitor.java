package serialMonitor;

import java.awt.BorderLayout;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;

public final class Monitor {

	public SerialMonitor serial;
	private JTextArea textArea;

	public Monitor() {
		JButton btnOpen = new JButton("Open Garage");
		JButton btnStop = new JButton("Stop Car");
		textArea = new JTextArea(20, 40);
		textArea.setEditable(false);
		JScrollPane scrollPnl = new JScrollPane(textArea);
		scrollPnl.setFocusable(false);
		DefaultCaret caret = (DefaultCaret)textArea.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		JFrame frame = new JFrame("Serial Monitor");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel panel = new JPanel(new BorderLayout());
		JPanel btnPanel = new JPanel();

		btnPanel.add(btnOpen);
		btnPanel.add(btnStop);

		btnOpen.addActionListener(e->{
			try {
				serial.sendData('O');

			} catch (IOException e1) {

				e1.printStackTrace();
			}
		});

		btnStop.addActionListener(e->{
			try {
				serial.sendData('S');
			} catch (IOException e1) {

				e1.printStackTrace();
			}
		});

		panel.add(scrollPnl, BorderLayout.CENTER);
		panel.add(btnPanel, BorderLayout.SOUTH);
		frame.getContentPane().add(panel);
		frame.pack();
		frame.setVisible(true);

	}
	public static void main(String[] args) {
		Monitor monitor = new Monitor();
		monitor.serial = new SerialMonitor(monitor);
		monitor.serial.start(args[0], Integer.parseInt(args[1]));
	}

	public void addData(String data) {
		textArea.append(data + '\n');
	}
}
