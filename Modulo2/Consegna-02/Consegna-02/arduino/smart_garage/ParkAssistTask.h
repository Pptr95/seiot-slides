#ifndef __PARKASSISTTASK__
#define __PARKASSISTTASK__

#include "Task.h"
#include "LedExt.h"
#include "Sonar.h"
#include "ButtonImpl.h"

class ParkAssistTask: public Task {

  private:
    int trigPin;
    int echoPin;
    int ledDist1Pin;
    int ledDist2Pin;
    int touchPin;
    int closePin;
    LightExt* ledDist1;
    LightExt* ledDist2;
    ButtonImpl* btnTouch;
    ButtonImpl* btnClose;
    Sonar* proxSensor;
    
  public:
    ParkAssistTask(int trigPin, int echoPin, int ledDist1Pin, int ledDist2Pin, int touchPin, int closePin);
    void init(int period);
    void tick();
};

#endif
