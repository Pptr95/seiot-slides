#ifndef __WAITCARTASK__
#define __WAITCARTASK__

#include "Task.h"
#include "LedExt.h"
#include "ButtonImpl.h"
#include "PirImpl.h"

class WaitCarTask: public Task {
  
  private:
    LightExt* ledRed;
    ButtonImpl* btnClose;
    PirImpl* pir;
    int ledRedPin;
    int pirPin;
    int closePin;
    int brightness;
    int fadeAmount;
    bool waitingCar;
    unsigned long int startTime;
    enum {CLOSED, OPENING, WAITCAR, DETECTED, CLOSING} state;
    
  public:
    WaitCarTask(int ledRedPin, int pirPin, int closePin);
    void init(int period);
    void tick();
};

#endif
