#include "Arduino.h"
#include "WaitCarTask.h"

#define WAIT_TIME 10000
#define DIST_MIN 0.1 // DIST_MIN, DIST_CLOSE e distance (variabile condivisa) non vengono utilizzate!!!!!!!!!!!!!!!
#define DIST_CLOSE 0.5

extern bool park;
extern float distance;

WaitCarTask::WaitCarTask(int ledRedPin, int pirPin, int closePin) {
  this->ledRedPin = ledRedPin;
  this->pirPin = pirPin;
  this->closePin = closePin;
}

void WaitCarTask::init(int period) {
  Task::init(period);
  Serial.begin(9600);
  brightness = 0;
  fadeAmount = 5;
  ledRed = new LedExt(ledRedPin, brightness);
  btnClose = new ButtonImpl(closePin);
  pir = new PirImpl(pirPin);
  pir->init();
  park = false;
  waitingCar = false;
  state = CLOSED;
}

void WaitCarTask::tick() {
  switch(state) {
    case CLOSED:
      if(Serial.available()) {
        char data = Serial.read();
        if(data == 'O') {
          state = OPENING;
        }
      }
      break;  
    case OPENING:
      if(brightness == 0) {
        ledRed->switchOn();
      }
      brightness = brightness + fadeAmount;
      ledRed->setIntensity(brightness);
      if(brightness == 255) {//Il led ci deve mettere 2 secondi per accendersi. Essendo che la macchina a stati ha un periodo di 40ms, per accendersi farà circa 51 (255 / 5) tick (aggiungendo ad ogni tick un +5 all'intensità).
        state = WAITCAR;     //Considerando questi valori, il led ci metterà 0,040 * 51 = 2,040 sec.
      }
      break;
    case WAITCAR:
      if(!waitingCar) {
        startTime = millis();
        waitingCar = true;
      }
      if(millis() - startTime >=  WAIT_TIME || btnClose->isPressed()) { 
        waitingCar = false;
        state = CLOSING;
      } else if (pir->isDetected()) {
        waitingCar = false;
        state = DETECTED;
        Serial.println("Welcome Home.");
        park = true;
      }
      break;
    case DETECTED:
      if(!park) {
        state = CLOSING;
      }
      break;
    case CLOSING:
      park = false; //variabile che viene messa costantemente a false anche quando lo è già in questo stato!!!!!!!!!!!!
      brightness = brightness - fadeAmount;
      ledRed->setIntensity(brightness);
      if(brightness == 0) {
        ledRed->switchOff();
        state = CLOSED;
      }
      break;
  }
}

