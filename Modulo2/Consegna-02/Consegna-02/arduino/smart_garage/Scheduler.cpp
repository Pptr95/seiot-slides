#include "Scheduler.h"

extern bool park;

void Scheduler::init(int basePeriod){
  this->basePeriod = basePeriod;
  timer.setupPeriod(basePeriod);  
  nTasks = 0;
}

bool Scheduler::addTask(Task* task){
  if (nTasks < MAX_TASKS){
    taskList[nTasks] = task;
    nTasks++;
    return true;
  } else {
    return false; 
  }
}
  
void Scheduler::schedule(){
  timer.waitForNextTick();
  taskList[0]->tick(); //quando la variabile condivisa park è a true, faccio fare comunque fare tick al task WaitCar ma lui non fa niente nello stato in cui è (Detected), tanto valeva fare il controllo se park è a true, non fargli fare tick
  if(park && taskList[1]->updateAndCheckTime(basePeriod)) {
     taskList[1]->tick();
  }
}

