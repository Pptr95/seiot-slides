#include "Arduino.h"
#include "ParkAssistTask.h"

#define DIST_MIN 0.1
#define DIST_MAX 1
#define DIST_CLOSE 0.5

extern bool park;
extern float distance;

ParkAssistTask::ParkAssistTask(int trigPin, int echoPin, int ledDist1Pin, int ledDist2Pin, int touchPin, int closePin) {
  this->trigPin = trigPin;
  this->echoPin = echoPin;
  this->ledDist1Pin = ledDist1Pin;
  this->ledDist2Pin = ledDist2Pin;
  this->touchPin = touchPin;
  this->closePin = closePin;
}

void ParkAssistTask::init(int period) {
  Task::init(period);
  Serial.begin(9600);
  ledDist1 = new LedExt(ledDist1Pin, 0);
  ledDist2 = new LedExt(ledDist2Pin, 0);
  btnTouch = new ButtonImpl(touchPin);
  btnClose = new ButtonImpl(closePin);
  proxSensor = new Sonar(echoPin, trigPin);
}
//pericolo overrun essendo che la computazione nello stato di parking è elevata (considerando il caso peggiore - WCET)
void ParkAssistTask::tick() { //qui in realtà gli stati (idle e parking) non sono stati rappresentati. La computazione nel metodo tick fa riferimento al solo stato parking. Quando la macchina a stati si trova nello stato di idle, non viene chiamata dallo scheduler 
  distance = proxSensor->getDistance(); //perchè la variabile condivisa park è a false. Avrei potuto creare gli stati espliciti e, al posto di far verificare allo scheduler se la variabile condivisa park era true, lo verificavo nello stato di idle.
  int tmpDist = distance * 100;
  Serial.print("Distance: ");
  Serial.println(distance);
  if(distance < DIST_MIN) {
    tmpDist = DIST_MIN * 100;
    Serial.println("OK CAN STOP");
  }
  if(distance > DIST_MAX) {
    tmpDist = DIST_MAX * 100;
    ledDist1->switchOff();
  }
  int minValue = DIST_MIN * 100;
  int maxValue = DIST_MAX * 100;
  int ledBrightness = map(tmpDist, minValue, maxValue, 510, 0);
  if(ledBrightness>255) {
    ledDist1->setIntensity(255);
    ledDist1->switchOn();
    ledDist2->setIntensity(ledBrightness-255);
    ledDist2->switchOn();
  } else {
    ledDist1->setIntensity(ledBrightness);
    ledDist1->switchOn();
    ledDist2->switchOff();
  }
  if(btnTouch->isPressed() && distance < DIST_MIN) {
    Serial.println("TOUCHING");
  }
  char data;
  if(Serial.available()) {
    data = Serial.read();
  }
  if(distance < DIST_MIN && data == 'S') {
    Serial.println("OK");
    ledDist1->switchOff();
    ledDist2->switchOff();
    park = false;
  } else if(distance > DIST_CLOSE && data == 'S') {
    Serial.println("TOO FAR");
  } else if(distance < DIST_CLOSE && btnClose->isPressed()) {
    ledDist1->switchOff();
    ledDist2->switchOff();
    park = false;
  }
}

