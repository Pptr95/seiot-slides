#include "WaitCarTask.h"
#include "ParkAssistTask.h"
#include "Scheduler.h"

float distance;
bool park;
const int basePeriod = 40;
Scheduler sched;
Task* t0;
Task* t1;

void setup() {
  sched.init(basePeriod);
  t0 = new WaitCarTask(11, 12, 3);
  t0->init(basePeriod);
  sched.addTask(t0);
  t1 = new ParkAssistTask(7, 4, 5, 6, 2, 3);//entrambi i task prendono in ingresso il bottone btnClose. Si sarebbe potuto implementare come oggetto condiviso singolo (non due istanze diverse dello stesso bottone sullo stesso pin).
  t1->init(200);
  sched.addTask(t1);
}

void loop() {
  sched.schedule();
}

