#ifndef __FOLLOW_THE_LIGHT__
#define __FOLLOW_THE_LIGHT__

/*Initializes the game*/
void initGame();

/*Waits player to start the game*/
void waitStart();

/*Shows the sequence to be guessed*/
void showSequence();

/*Waits player to guess the sequence shown*/
void waitForPlayer();

/*Turns on and off the led*/
void turnLed(int buttonID);

/*Checks if the button pressed is the right one*/
void checkButton(int buttonID);

/*Checks if the game is over*/
bool isGameOver();

/*Ends the game*/
void gameOver();

#endif
