/* Programma realizzato da
    Saccomanni Matteo
    Grossi Giulio
    Potrimba Petru
*/

#include "follow_the_light.h"

#define LED1_PIN 11
#define LED2_PIN 12
#define LED3_PIN 13
#define LED_FLASH_PIN 6
#define BUTTON1_PIN 2
#define BUTTON2_PIN 3
#define BUTTON3_PIN 4
#define POT_PIN A0
#define MAX_LENGTH 200
#define LED_DELAY 100


void setup() {
  pinMode(LED1_PIN, OUTPUT);
  pinMode(LED2_PIN, OUTPUT);
  pinMode(LED3_PIN, OUTPUT);
  pinMode(LED_FLASH_PIN, OUTPUT);
  pinMode(BUTTON1_PIN, INPUT);
  pinMode(BUTTON2_PIN, INPUT);
  pinMode(BUTTON3_PIN, INPUT);
  pinMode(POT_PIN, INPUT);
  randomSeed(analogRead(0));
  Serial.begin(9600);
}

void loop() {
  initGame();
  waitStart();
  while (!isGameOver()) {
    showSequence();
    waitForPlayer();
  }
}

