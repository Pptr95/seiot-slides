/* Programma realizzato da
 *  Saccomanni Matteo
 *  Grossi Giulio
 *  Potrimba Petru
 */

#include "follow_the_light.h"
#include <Arduino.h>

#define LED1_PIN 11
#define LED2_PIN 12
#define LED3_PIN 13
#define LED_FLASH_PIN 6
#define BUTTON1_PIN 2
#define BUTTON2_PIN 3
#define BUTTON3_PIN 4
#define POT_PIN A0
#define MAX_LENGTH 200
#define LED_DELAY 100

bool over;
int ledTime;
int sequence[MAX_LENGTH];
int score;
int currLength;
int pos;
int potValue;
unsigned long int timeout;

void initGame() 
{
  over = false;
  score = 0;
  pos = 0;
  currLength = 0;
  Serial.println("Welcome to Follow the Light!");
}

void waitStart()
{
    int fadeAmount = 5;
    int currIntensity = 0;
    while (digitalRead(BUTTON1_PIN) != HIGH) {
        currIntensity = currIntensity + fadeAmount;
        analogWrite(LED_FLASH_PIN, currIntensity);
        if (currIntensity == 0 || currIntensity == 255) {
            fadeAmount = -fadeAmount;
        }
        delay(20);
    }
    int sensorValue = analogRead(POT_PIN);
    potValue = map(sensorValue, 0, 1023, 1, 10);
    digitalWrite(LED_FLASH_PIN, LOW);
    Serial.println("Ready!");
    ledTime = map(sensorValue, 0, 1023, 400, 200); //frequenza con cui il led rimane acceso
    delay(1000);
}

void showSequence()
{
    int randNumber = random(BUTTON1_PIN, (BUTTON3_PIN + 1));
    sequence[currLength] = randNumber;
    currLength++;
    timeout = 2000 * currLength; //tempo che il giocatore ha per rappresentare la sequenza
    for (int i = 0; i < currLength; i++) {
        turnLed(sequence[i]);
    }
}

void waitForPlayer()
{
    pos = 0;
    unsigned long int startTime = millis();
    while (pos < currLength && !over) {
        for (int i = BUTTON1_PIN; i <= BUTTON3_PIN && !over; i++) { //mi guardo per ogni bottone che clicca il giocatore
            if (digitalRead(i) == HIGH) { // se è il bottone nella sequenza esatta e controllo ad ogni click con !over se ha  
                turnLed(i); //cliccato il bottone corretto o meno 
                checkButton(i);
            }
        }
        if((millis() - startTime) > timeout){
          gameOver();
        }
    }
    if (!over) { // esco dal while o se ho perso oppure se ho indovinato la sequenza, se ho indovinato la sequenza, e quindi non ho perso, aumenta lo score
        score += currLength;
        if (currLength == MAX_LENGTH) { // se sono arrivato alla 200esima sequenza, finisci il gioco
            gameOver();
        }
        delay(1000);
    }
}

void turnLed(int buttonID)
{
    int ledID;
    switch (buttonID) { //in base al tipo di bottone, accendo il led corrispondente
    case BUTTON1_PIN:
        ledID = LED1_PIN;
        break;
    case BUTTON2_PIN:
        ledID = LED2_PIN;
        break;
    case BUTTON3_PIN:
        ledID = LED3_PIN;
        break;
    }
    digitalWrite(ledID, HIGH);
    delay(ledTime);
    digitalWrite(ledID, LOW);
    delay(LED_DELAY);
}

void checkButton(int buttonID)
{
    if (sequence[pos] != buttonID) {
        gameOver();
    }
    else {
        pos++;
    }
}

bool isGameOver() {
    return over;
}

void gameOver()
{  
  Serial.print("Game Over - Score: ");
  Serial.println(score*potValue);  
  over = true;
  digitalWrite(LED_FLASH_PIN, HIGH);
  delay(2000);
  digitalWrite(LED_FLASH_PIN, LOW);
  delay(1500);
}

